package com.example.apphola93;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class spinnerAdapter extends ArrayAdapter<ItemData> {

    private final ArrayList<ItemData> list;
    int groupId;

    Activity Context;
    ArrayList<ItemData> List;
    LayoutInflater inflater;

    public spinnerAdapter(Activity Context, int groupId, int id, ArrayList<ItemData> list){

        super(Context, id, list);
        this.list = list;
        inflater = (LayoutInflater) Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;

    }

    public View getView(int posicion, View convertView, ViewGroup parent){

        View itemView = inflater.inflate(groupId, parent, false);

        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgCategoria);
        imagen.setImageResource(list.get(posicion).getImageId());

        TextView txtCategoria = (TextView) itemView.findViewById(R.id.txtCategorias);
        txtCategoria.setText(list.get(posicion).getTxtCategoria());

        TextView txtDescripcion = (TextView) itemView.findViewById(R.id.txtDescripcion);
        txtDescripcion.setText(list.get(posicion).getTxtDescripcion());

        return itemView;
    }

    public View getDropDownView(int posicion, View convertView, ViewGroup parent){
        return getView(posicion, convertView, parent);
    }
}
